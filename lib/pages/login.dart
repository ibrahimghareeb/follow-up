import 'package:flutter/material.dart';
import 'package:follow_up/model/login_model.dart';
import 'package:follow_up/pages/home_page.dart';
import 'package:follow_up/pages/signup.dart';
import 'package:follow_up/utilis/conection_manger.dart';
import 'package:follow_up/utilis/response.dart';
import 'package:follow_up/widget/button.dart';
import 'package:follow_up/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatelessWidget {

  Login({Key? key}) : super(key: key);

  final TextEditingController username=TextEditingController();
  final TextEditingController universityId=TextEditingController();
  final formKey=GlobalKey<FormState>();
  String? validateUsername(String email){
    if (email.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }
  String? validateUniversityId(String password){
    if (password.isEmpty) {
      return "please enter university id";
    } else if (password.length<4) {
      return "university id length must be 4 character ";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: width,
          height: height,
          color: Color.fromRGBO(248, 251, 255, 1),
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: height*0.05,),
                  Image.asset("assets/images/Logo.png",width: width*0.8,),
                  SizedBox(height: height*0.1,),
                  Padding(
                    padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                    child: TextInput(controller: username, textInputType: TextInputType.emailAddress, hintText: "Username", validate:(value) {return validateUsername(value!);}),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
                    child: TextInputPassword(controller: universityId, textInputType: TextInputType.visiblePassword, hintText: "University id", validate:(value) {return validateUniversityId(value!);}),
                  ),
                  Container(margin: EdgeInsets.only(top: height*0.05),child: ButtonFormWidget(minWidth: width*0.87, buttonText: "Login",
                    onPressed: () async{
                      if(formKey.currentState!.validate()){
                        Response? response = await login(username.text.toString(), universityId.text.toString());
                        if (response.statusCode == 200) {

                          LoginModel model = response.body;
                          if (model.success == 1) {
                            SharedPreferences temp = await SharedPreferences.getInstance();
                              temp.setBool("login", true);
                              temp.setString("username", username.text.toString());
                              temp.setString("universityId", universityId.text.toString());
                              temp.setString("id", model.id!);
                              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder) => HomePage()), ModalRoute.withName("/Home"));

                          }
                          else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(model.message!,)));
                          }
                        }
                      }
                      },)),
                  Container(margin: EdgeInsets.only(top: height*0.02),child: Row(mainAxisAlignment: MainAxisAlignment.center,children: [Text("if you don't have an account, ",style: TextStyle(fontFamily: "Montserrat"),),InkWell(child: Container(child: Text("Sign up!",style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(87, 148, 246, 1),fontFamily: "Montserrat"),)),onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> Signup()));},)],))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
