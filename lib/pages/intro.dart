import 'package:flutter/material.dart';
import 'package:follow_up/pages/login.dart';
import 'package:steps_indicator/steps_indicator.dart';

class Intro extends StatefulWidget {



  @override
  State<Intro> createState() => _IntroState();
}

class _IntroState extends State<Intro> {

  int introCounter=0;

  String selectedText="Follow-up of students' attendance";

  @override
  Widget build(BuildContext context) {

    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/images/intro.png")
            )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                SizedBox(height: height*0.6,),
                Text(selectedText,style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.bold,fontSize: 22,color: Colors.white),textAlign: TextAlign.center,),

              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: width*0.05,right:width*0.05 ,bottom: height*0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    child: Text("skip",style:TextStyle(fontFamily: "Montserrat",fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold)),
                    onTap: (){
                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Login()),ModalRoute.withName("/login"));
                    },
                  ),
                  StepsIndicator(
                    selectedStepColorIn: Colors.white,
                    // undoneLineColor: Colors.white,
                    // doneLineColor: Colors.white,
                    undoneLineThickness: 0,
                    doneLineThickness: 0.0,
                    enableStepAnimation: true,
                    selectedStepSize: 15,
                    doneStepSize: 10,
                    unselectedStepSize: 10,
                    lineLength: 20,
                    selectedStepColorOut: Colors.white,
                    unselectedStepColorOut: Colors.grey,
                    unselectedStepColorIn: Colors.grey,
                    doneStepColor: Colors.grey,
                    selectedStep: introCounter,
                    nbSteps: 3,
                  ),
                  InkWell(
                    child: Text("Next",style:TextStyle(fontFamily: "Montserrat",fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold)),
                    onTap: (){
                      introCounter=(++introCounter)%3;
                      if(introCounter==0) {
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Login()),ModalRoute.withName("/login"));
                      }
                      if(introCounter==1){
                        setState(() {
                          selectedText="Follow up on all the details of interest \n to the student";
                        });
                      }
                      if(introCounter==2){
                        setState(() {
                          selectedText="Review the number of absences";
                        });
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
