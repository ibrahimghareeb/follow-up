import 'package:flutter/material.dart';
import 'package:follow_up/model/login_model.dart';
import 'package:follow_up/pages/login.dart';
import 'package:follow_up/utilis/conection_manger.dart';
import 'package:follow_up/utilis/response.dart';
import 'package:follow_up/widget/button.dart';
import 'package:follow_up/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {

  bool edit=false;

  final TextEditingController username=TextEditingController();
  final TextEditingController phone=TextEditingController();

  final formKey=GlobalKey<FormState>();

  String? name;
  String? userId;

  String? validateUsername(String email){
    if (email.isEmpty) {
      return "please enter username";
    } else {
      return null;
    }
  }

  String? validatePhone(String phone){
    if (phone.isEmpty) {
      return "please enter phone number";
    } else if (phone.length<8) {
      return "please enter valid phone number ";
    } else {
      return null;
    }
  }

  @override
  void initState() {
    SharedPreferences.getInstance().then((value) {
      name=value.getString("username");
      userId=value.getString("id");
      setState(() {

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/images/edit_profile.png")
          )
        ),
        child: !edit?showInfo(width, height):editInfo(width, height)
      ),
    );
  }

  Widget showInfo(double width,double height){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: width,
          height: height*0.2,
          margin: EdgeInsets.symmetric(horizontal: width*0.05),
          decoration: BoxDecoration(
            border: Border.all(color: Color.fromRGBO(87, 148, 246, 1),width: 2),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(name==null?"":name!,style: TextStyle(fontFamily: "Montserrat",fontSize: 20,fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 128, 235, 1)),),
              Text(userId==null?"":userId!,style: TextStyle(fontFamily: "Montserrat",color: Color.fromRGBO(135, 135, 135, 1)),),
              //Text("User phone",style: TextStyle(fontFamily: "Montserrat",color: Color.fromRGBO(135, 135, 135, 1)),),
            ],
          ),
        ),
        Container(margin: EdgeInsets.only(top: height*0.05),child: ButtonFormWidget(minWidth: width*0.87, buttonText: "Edit",onPressed: (){setState(() {edit=!edit;});},)),
        Container(margin: EdgeInsets.only(top: height*0.05),child: ButtonFormWidget(minWidth: width*0.87, buttonText: "Logout",onPressed: ()async{
          SharedPreferences temp = await SharedPreferences.getInstance();
          temp.setBool("login", false);
          temp.remove("username");
          temp.remove("universityId");
          temp.remove("id");
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder) => Login()), ModalRoute.withName("/Login"));
        },)),
      ],
    );
  }
  Widget editInfo(double width,double height){
    return Form(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
            child: TextInput(controller: username, textInputType: TextInputType.emailAddress, hintText: "Username", validate:(value) {return validateUsername(value!);}),
          ),
          Padding(
            padding: EdgeInsets.only(left: width*0.05,right: width*0.05),
            child: TextInput(controller: phone, textInputType: TextInputType.phone, hintText: "Phone number", validate:(value) {return validatePhone(value!);}),
          ),
          Container(margin: EdgeInsets.only(top: height*0.05),child: ButtonFormWidget(minWidth: width*0.87, buttonText: "Save",onPressed: ()async{
              SharedPreferences data=await SharedPreferences.getInstance();
              Response response=await editAccount(username.text.toString(), phone.text.toString(), data.getString("id")!);
              if(response.statusCode==200){
                LoginModel model=response.body;
                if(model.success==1){
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(model.message!,)));
                  setState(() {
                    edit=!edit;
                  });
                }else{
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(model.message!,)));
                }
              }

            },)),
        ],
      ),
    );
  }
}
