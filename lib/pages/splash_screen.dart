import 'package:flutter/material.dart';
import 'package:follow_up/pages/home_page.dart';
import 'package:follow_up/pages/intro.dart';
import 'package:follow_up/pages/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}



class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    SharedPreferences.getInstance().then((data) {
      if(!data.containsKey("first time")){
          data.setBool("first time", true);
          Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Intro()),ModalRoute.withName("/intro")));
      }
      else if(data.containsKey("login")&&data.getBool("login")!){
          Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> HomePage()),ModalRoute.withName("/home")));
        }
      else{
        Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (builder)=> Login()),ModalRoute.withName("/login")));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: width,
          height: height,
          decoration:const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/images/splash_screen.png",)
            )
          ),
        ),
      ),
    );
  }
}
