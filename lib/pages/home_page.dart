import 'package:flutter/material.dart';
import 'package:follow_up/pages/number_of_absences.dart';
import 'package:follow_up/pages/profile_screen.dart';
import 'package:follow_up/pages/search_result.dart';
import 'package:follow_up/widget/button.dart';
import 'package:follow_up/widget/text_field.dart';

class HomePage extends StatelessWidget {

  HomePage({Key? key}) : super(key: key);

  final TextEditingController search=TextEditingController();

  final formKey=GlobalKey<FormState>();

  String? validateSearch(String email){
    if (email.isEmpty) {
      return "please enter text";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            color: Color.fromRGBO(248, 251, 255, 1),
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/images/home.png")
            )
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              SizedBox(height: height*0.02,),
              Form(
                key: formKey,
                child: Container(
                  width: width*0.6,
                  child: Row(
                    children: [
                      Expanded(child: TextInput(controller: search, textInputType: TextInputType.text, hintText: "Search for specific subject", validate:(value) {return validateSearch(value!);})),
                      InkWell(child: Icon(Icons.search,size: 40,color: Colors.white,),
                        onTap: (){
                          if(formKey.currentState!.validate()){
                            Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> SearchResult()));
                          }
                        },
                      )
                    ],
                  )
                ),
              ),
              SizedBox(height: height*0.1,),
              Container(
                padding: EdgeInsets.symmetric(horizontal: width*0.05),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: Card(
                        elevation: 10,
                        shape: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide: BorderSide.none
                        ),
                        child: Container(
                          width: width*0.4,
                          height: width*0.3,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Number of absences",style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1)))
                            ],
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> NumberOfAbsences()));
                      },
                    ),
                    InkWell(
                      child: Card(
                        elevation: 10,
                        shape: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            borderSide: BorderSide.none
                        ),
                        child: Container(
                          width: width*0.4,
                          height: width*0.3,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Profile",style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1)))
                            ],
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (builder)=> EditProfile()));
                      },
                    )
                  ],
                ),
              ),
              SizedBox(height: height*0.1,),
              Container(
                width: width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                   Text("Scan the barcode to prove attendance",style: TextStyle(fontFamily: "Montserrat",fontSize: 18,fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1))),
                   SizedBox(height: height*0.04,),
                   Padding(
                     padding:  EdgeInsets.symmetric(horizontal: width*0.05),
                     child: MaterialButton(
                       minWidth: width*0.87,
                       height: 100,
                       color: Color.fromRGBO(87, 148, 246, 1),
                       shape:  OutlineInputBorder(
                           borderRadius: BorderRadius.all(Radius.circular(25)),
                           borderSide: BorderSide.none
                       ),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Image.asset("assets/images/barcode.png",width: 100,height: 50,),
                           Text("SCAN",style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.bold,color: Colors.white,fontSize: 25))
                         ],
                       ),
                       onPressed: (){}
                     ),
                   ),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
