import 'package:flutter/material.dart';

class SearchResult extends StatefulWidget {
  const SearchResult({Key? key}) : super(key: key);

  @override
  State<SearchResult> createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        color: Color.fromRGBO(248, 251, 255, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: height*0.05,),
            Image.asset("assets/images/Logo.png",width: width*0.8,),
            SizedBox(height: height*0.05,),
            Text("Search result",style: TextStyle(fontFamily: "Montserrat",fontSize: 25,fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1))),
            SizedBox(height: height*0.05,),
            Card(
              elevation: 10,
              shape: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Container(
                padding: EdgeInsets.all(30),
                //width: width*0.4,
                child: Text("Number of absences : 10",style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1))),
              ),
            )
          ],
        ),
      ),
    );
  }
}
