import 'package:flutter/material.dart';
import 'package:follow_up/model/number_of_absence.dart';
import 'package:follow_up/utilis/conection_manger.dart';
import 'package:follow_up/utilis/response.dart';
import 'package:follow_up/widget/absence.dart';
import 'package:shared_preferences/shared_preferences.dart';


class NumberOfAbsences extends StatefulWidget {
  const NumberOfAbsences({Key? key}) : super(key: key);

  @override
  State<NumberOfAbsences> createState() => _NumberOfAbsencesState();
}

class _NumberOfAbsencesState extends State<NumberOfAbsences> {

  List<NumberOfAbsenceModel>? absenceList;
  bool isLoaded=false;

  @override
  void initState() {
    getAbsenceFromAPI();
    super.initState();
  }

  Future<void> getAbsenceFromAPI() async{
    SharedPreferences data=await SharedPreferences.getInstance();
    Response response=await getNumberOfAbsence(data.getString("id")!);
    if(response.statusCode==200){
      absenceList=response.body;
    }
    setState(() {
      isLoaded=true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(73, 130, 243, 1),
        title: Text("Number of absences"),
      ),
      body: Container(

        margin: EdgeInsets.only(left: width*0.05,right: width*0.05,top: height*0.05),
        width: width,
        height: height,
        color: Color.fromRGBO(248, 251, 255, 1),
        child: Column(
            mainAxisAlignment: isLoaded&&absenceList!=null?MainAxisAlignment.start:MainAxisAlignment.center,
            children: !isLoaded?[Container(width:width,child: Row(children: [CircularProgressIndicator(color: Color.fromRGBO(87, 148, 246, 1),)],mainAxisAlignment: MainAxisAlignment.center,))]:
            absenceList!.isEmpty?[Text("there is no absence")]:
            absenceList!.map((e) => Absence(model: e)).toList()

        ),
      ),
    );
  }
}
