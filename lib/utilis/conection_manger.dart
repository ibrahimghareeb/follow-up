import 'dart:convert';

import 'package:follow_up/model/login_model.dart';
import 'package:follow_up/model/number_of_absence.dart';
import 'package:follow_up/pages/number_of_absences.dart';
import 'package:follow_up/utilis/response.dart';
import 'package:http/http.dart' as http;

final String baseUrl="https://cosale.000webhostapp.com";

Future <Response> login(String username, String universityId) async{
  var url=Uri.parse("$baseUrl/login1.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: jsonEncode(<String, String>{
    "username":username,
    "universityId":universityId
  }),);
  var data=json.decode(response.body.toString());
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  return Response(statusCode: 404,body: "login failed");
}
Future <Response> signup(String username, String universityId,String phone) async{
  var url=Uri.parse("$baseUrl/register1.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  }, body:  jsonEncode(<String, String>{
    "username": username,
    "universityId": universityId,
    "role":"student",
    "phoneNumber":phone
  }));
  print(response.body);
  var data=json.decode(response.body);
  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  else if (response.statusCode==401){
    return Response(statusCode: 401,body: "register failed");
  }
  return Response();
}

Future <Response> editAccount(String username, String phone,String id) async{
  var url=Uri.parse("$baseUrl/editProfile.php");
  http.Response response = await http.post(url, headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  },body: {
    "username":username,
    "phoneNumber":phone,
    "id":id
  });
  var data=json.decode(response.body);

  if(response.statusCode==200){
    LoginModel model=LoginModel.fromJson(data);
    return Response(statusCode: 200,body: model);
  }
  else if (response.statusCode==401){
    return Response(statusCode: 401,body: "edit account failed");
  }
  return Response();
}

Future<Response> getNumberOfAbsence(String id)async {
  var url=Uri.parse("$baseUrl/getNumberOfAbsence.php?id=$id");
  http.Response response = await http.get(url,headers: {
    'content_type': 'application/json',
    'accept': 'application/json'
  });
  if(response.body.length==0){
    List<NumberOfAbsences> list=[];
    return Response(statusCode: 200,body: list);
  }
  var data=json.decode(response.body.toString());
  if(response.statusCode==200 ){
    List list=(data as List).map((e) => NumberOfAbsenceModel.fromJson(e)).toList();
    return Response(statusCode: 200,body: list);
  }
  return Response(statusCode: 404,body: "can't get order");
}