class NumberOfAbsenceModel{
  int? number;
  String? subjectName;
  String? studentId;
  String? date;

  NumberOfAbsenceModel.fromJson(Map<String,dynamic> json){
    number=json["absence"];
    subjectName=json["subjectName"];
    studentId=json["student_id"];
    date=json["date"];
  }

}