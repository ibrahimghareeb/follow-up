class LoginModel{
  String? id;
  int? success;
  int? status;
  String? role;
  String? message;

  LoginModel.fromJson(Map<String,dynamic> json){
    id=json["id"];
    success=json["success"];
    role=json["role"];
    status=json["status"];
    message=json["message"];
  }
}