
import 'package:flutter/material.dart';



class ButtonFormWidget extends StatelessWidget {
  late final GestureTapCallback  onPressed;
  late double minWidth;
  late String buttonText;


  ButtonFormWidget({required this.minWidth,required this.buttonText,required this.onPressed});


  @override
  Widget build(BuildContext context) {
    return  MaterialButton(
      minWidth: minWidth,
      height: 40,
      child:  Text(buttonText,style: TextStyle(
          color: Colors.white,
          fontSize: 15
      ),),
      color: Color.fromRGBO(87, 148, 246, 1),
      shape:  OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        borderSide: BorderSide.none
      ),
      onPressed: onPressed,


    );
  }
}
