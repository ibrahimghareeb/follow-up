import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class TextInput extends StatelessWidget {
  late final TextEditingController controller;
  late TextInputType textInputType;
  late String hintText;
  late Color? backgroundColor=Colors.white;
  late final String? Function(String?)? validate;
  late int? maxLine=1;
  late final GestureTapCallback? onEditingComplete;
  TextInput({required this.controller,required this.textInputType,required this.hintText,required this.validate,this.maxLine,this.backgroundColor,this.onEditingComplete,this.isEnabled});
  bool? isEnabled;
  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(5 ,15, 5, 5),
      child: TextFormField(
        enabled: isEnabled!=null? isEnabled:true,
        controller: controller,
        keyboardType: textInputType,
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
        ),
        maxLines: maxLine,
        onEditingComplete: onEditingComplete!=null?onEditingComplete:(){},
        decoration: InputDecoration(
          filled: true,
          fillColor: backgroundColor!=null?backgroundColor:Colors.white,
          contentPadding: EdgeInsets.only(left: width*0.06),
          hintText: hintText,
          hintStyle: TextStyle(
            fontSize: 12,
            color: Colors.grey,
          ),

          errorStyle: TextStyle(
              fontSize: 12,
              color: Colors.red,
            overflow: TextOverflow.clip
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 0,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.red,
              )
          ),

        ),
        validator: validate,


      ),
    );
  }
}


class TextInputPassword extends StatefulWidget {
  late final TextEditingController controller;
  late TextInputType textInputType;
  late String hintText;
  late final String? Function(String?)? validate;
  TextInputPassword({required this.controller,required this.textInputType,required this.hintText,required this.validate});

  @override
  State<TextInputPassword> createState() => _TextInputPasswordState();
}

class _TextInputPasswordState extends State<TextInputPassword> {
  late bool obscure=true;
  late bool ceckboxVal=false;
  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.fromLTRB(5, 15, 5, 5),
      child: TextFormField(
        obscureText: obscure,
        controller: widget.controller,
        keyboardType: widget.textInputType,
        style: TextStyle(
          fontSize: 12,
          color: Colors.black,
        ),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          suffixIcon: IconButton(icon: Icon(obscure?Icons.remove_red_eye:Icons.visibility_off,size: 20,color: Color(0xFF303030),),onPressed: (){
            setState(() {
              obscure=!obscure;
            });
          },),
          contentPadding: EdgeInsets.only(left: width*0.06),
          hintText: widget.hintText,
          hintStyle: TextStyle(
            fontSize: 12,
            color: Colors.grey,
          ),
          errorStyle: TextStyle(
              fontSize: 12,
              color: Colors.red
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey.withOpacity(0.5),
              )
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              borderSide: BorderSide(
                width: 1,
                color: Colors.red,
              )
          ),
        ),
        validator: widget.validate,

      ),
    );
  }
}
