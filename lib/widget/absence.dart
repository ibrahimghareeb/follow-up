import 'package:flutter/material.dart';
import 'package:follow_up/model/number_of_absence.dart';

class Absence extends StatelessWidget {
  Absence({Key? key,required this.model}) : super(key: key);

  NumberOfAbsenceModel model;

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Card(
      margin: EdgeInsets.only(bottom: height*0.02),
      elevation: 10,
      shape: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        borderSide: BorderSide.none
      ),
      child: Container(
        width: width,
        height: height*0.15,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("${model.subjectName} : " ,style: TextStyle(fontFamily: "Montserrat",fontSize: 16,fontWeight: FontWeight.bold,color: Color.fromRGBO(135, 135, 135, 1))),
            Text("Number of absences = ${model.number}",style: TextStyle(fontFamily: "Montserrat",fontSize: 16,fontWeight: FontWeight.bold,color: Color.fromRGBO(87, 148, 246, 1)))
          ],
        ),
      ),
    );
  }
}
